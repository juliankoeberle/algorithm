package at.jaykay.Sort;

public class ArrayBubbleSort implements ArraySorter {

	@Override
	public int[] sort(int[] array) {
		int temp;
		for(int i=1; i<array.length; i++) {
			for(int j=0; j<array.length-i; j++) {
				if(array[j]>array[j+1]) {
					temp=array[j];
					array[j]=array[j+1];
					array[j+1]=temp;
				}
				
			}
		}
		return array;
	}
	

	@Override
	public String getName() {
		return "ArrayBubbleSort";
	}
	
}
