package at.jaykay.Sort;

import java.util.LinkedList;

public class MergeSort implements Sorter {
	
	private LinkedList<Integer> list = null;

	@Override
	public LinkedList<Integer> sort(LinkedList<Integer> list) {
		this.list = list;
		this.devide();
		return this.list;
	}
	
	//in alpha
	
	private void devide(){
		int helper2 = 1;
		int helper = 0;
		int size = this.list.size();
		while ((int)Math.pow(2, helper2) <= size ){
			System.out.println("hehe" + (int)Math.pow(2, helper2));
			for (int i = 0 ; i < this.list.size()-(int)Math.pow(2, helper) ; i+=2){
				//swaping 0 1
				//nexttim 0 2
				//then 0 4
				//then 0 8 ...
				this.swap(i, i+(int)Math.pow(2, helper));
			}
			helper++;
			helper2++;
		}
	}
	
	private boolean swap(int index1, int index2){
		int number1 = this.list.get(index1);
		int number2 = this.list.get(index2);
		if (number1 > number2){
			this.list.set(index1, number2);
			this.list.set(index2, number1);
			return false;
		}
		return true;
	}
	/*
	private void merge(){
		//quater block
		int size = this.list.size();
		int i = 0;
		int j = 2;
		if (size >= 4){
			//queck if the size size 4,8,16,32,64,128,...
			if ((Math.log(size)/Math.log(2)) % 2 == 0){
				while (i < 4){
					if (this.swap(i, j)){
						i++;
					}
				}
			}
		}
	}*/

	@Override
	public String getName() {
		return "MergeSort";
	}

}
