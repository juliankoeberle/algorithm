package at.jaykay.Sort;

import java.util.LinkedList;

public class BubbleSort implements Sorter {
	
	private LinkedList<Integer> list = null;

	@Override
	public LinkedList<Integer> sort(LinkedList<Integer> list) {
		this.list = list;
		int size = this.list.size();
		for (int j = 0 ; j < size ; j++){
			for(int i = 1 ; i < size ; i++){
				if (list.get(i-1)> list.get(i)){
					swap(i-1,i);
				}
			}
		}
		return list;
	}
	
	private void swap(int index1, int index2){
		int number1 = this.list.get(index1);
		int number2 = this.list.get(index2);
		this.list.set(index1, number2);
		this.list.set(index2, number1);
	}

	@Override
	public String getName() {
		return "BubbleSort";
	}
	

	

}
