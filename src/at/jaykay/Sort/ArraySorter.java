package at.jaykay.Sort;

public interface ArraySorter {
	public int[] sort(int[] array);
	public String getName();
}
