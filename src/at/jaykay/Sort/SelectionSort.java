package at.jaykay.Sort;

import java.util.LinkedList;


public class SelectionSort implements Sorter{
	
	private LinkedList<Integer> list;
	private LinkedList<Integer> newList;


	private int getTheSmallestNumber(){
		int helper = list.get(0);
		int size = list.size();
		int index = 0 ;
		for (int j = 0 ; j < size ; j++){
			if (helper > list.get(j)){
				helper = list.get(j);
				index = j;
			}
		}
		this.list.remove(index);
		return helper;
	}

	@Override
	public LinkedList<Integer> sort(LinkedList<Integer> list) {
		this.list = list;
		this.newList = new LinkedList<Integer>();
		while(!list.isEmpty()){
			this.newList.add(this.getTheSmallestNumber());
		}
		return newList;
		
	}

	@Override
	public String getName() {
		return "SelectionSort";
	}

}
