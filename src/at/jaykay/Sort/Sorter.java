package at.jaykay.Sort;

import java.util.LinkedList;

public interface Sorter {
	public LinkedList<Integer> sort(LinkedList<Integer> list);
	public String getName();
}
