package at.jaykay.List;
/**
 * Created by juliankoberle on 05/10/16.
 */
public class Node {

    private int value;
    private Node nextNode;
    private Node previousNode;

    public Node(){

    }

    public Node(int value, Node previousNode) {
        this.value = value;
        this.previousNode = previousNode;
    }

    public int getValue() {
        return value;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public Node getPreviousNode() {
        return previousNode;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
    }

    public void setPreviousNode(Node previousNode) {
        this.previousNode = previousNode;
    }
}
