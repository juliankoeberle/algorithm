package at.jaykay.List;
/**
 * Created by juliankoberle on 05/10/16.
 */
public class List {
    //null null null
    private Node root = null;

    public List(){

    }
    public void add(int value){
        if (root == null){
            root = new Node();
            root.setValue(value);
        }
        else{
            Node node = new Node(value,root);
            root.setNextNode(node);
            root = node;
        }

    }

    public int getElementByIndex(int index){
        int sizeOfList = size();
        Node ro = root;
        Node node;
        //because indexes start by 0;

        int i = 0;
        while (true){
            if (ro!= null) {
                node = ro.getPreviousNode();
                if ((sizeOfList-i-1) == index){
                    return ro.getValue();
                }
                ro = node;
                i++;
            }
            else{
                break;
            }
        }
        return -1;
    }
    public int removeElementByIndex(int index){
        int sizeOfList = size();
        Node root = this.root;
        Node node = null;
        //because indexes start by 0;

        int i = 0;
        while (true){
            if (root!= null) {
                node = root.getPreviousNode();
                if ((sizeOfList-i-1) == index) {
                    node.setNextNode(root.getNextNode());
                    System.out.println("node" + node.getValue());
                    System.out.println("root" + root.getNextNode().getValue());
                    break;
                }
                //node = root.getPreviousNode();
                root = node;
                i++;
            }
            else{
                break;
            }
        }
        return -1;
    }
    public int size(){
        Node ro = root;
        Node node;
        int i = 0;
        while (true){
            if (ro!= null) {
                node = ro.getPreviousNode();
                ro = node;
                i++;
            }
            else{
                break;
            }
        }
        return i;
    }
}
