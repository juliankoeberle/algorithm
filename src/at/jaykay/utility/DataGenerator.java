package at.jaykay.utility;

import java.util.LinkedList;
import java.util.Random;

public class DataGenerator {
	
	public static LinkedList<Integer> generateData(int min, int max, int size){
		LinkedList<Integer> list = new LinkedList<>();
		int random = 0;
		Random rand = new Random();
		for (int i = 0 ; i < size ; i++){
			random  = rand.nextInt((max - min) + 1) + min;
			list.add(random);
		}
		return list;
	}
	public static void printData(LinkedList<Integer> list){
		for (int i = 0 ; i < list.size(); i++){
			System.out.println(list.get(i));
		}
	}
	
	public static int[] generateDataArray(int min, int max, int size){
		int[] array = new int[size];
		int random = 0;
		Random rand = new Random();
		for (int i = 0 ; i < size ; i++){
			random  = rand.nextInt((max - min) + 1) + min;
			array[i] = random;
		}
		return array;
	}
	

}
