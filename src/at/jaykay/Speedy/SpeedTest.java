package at.jaykay.Speedy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import at.jaykay.Sort.ArraySorter;
import at.jaykay.Sort.BubbleSort;
import at.jaykay.Sort.SelectionSort;
import at.jaykay.Sort.Sorter;
import at.jaykay.utility.DataGenerator;

public class SpeedTest {
	private List<ArraySorter> algos;
	
	public SpeedTest() {
	      this.algos = new ArrayList<>();
	 }
	
	 public void addAlgorithm(ArraySorter algo) {
	      this.algos.add(algo);
	 }
	 
	 
	 public void run() {
	      int[] data = DataGenerator.generateDataArray(0, 1000, 10000);
	      
	      for (ArraySorter sorter : this.algos) {
	        System.out.println(sorter.getName());
	        
	        final long timeStart = System.currentTimeMillis(); 
	        sorter.sort(data); 
	        final long timeEnd = System.currentTimeMillis(); 
	        System.out.println("Delta Time " + (timeEnd - timeStart) + " Millisek.");
	      }
	  }
	
}


