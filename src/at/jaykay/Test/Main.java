package at.jaykay.Test;


import java.util.LinkedList;

import at.jaykay.Sort.ArrayBubbleSort;
import at.jaykay.Sort.ArrayMergeSort;
import at.jaykay.Sort.ArraySelectionSort;
import at.jaykay.Sort.BubbleSort;
import at.jaykay.Sort.SelectionSort;
import at.jaykay.Speedy.SpeedTest;
import at.jaykay.utility.DataGenerator;


public class Main {
	public static void main(String[] args){
		System.out.println("speed Test starting...");
		SpeedTest speedy = new SpeedTest();
		
		//LinkedList<Integer> list = DataGenerator.generateData(0, 100, 10000);
		//System.out.println("sorting");
		//DataGenerator.printData(new SelectionSort().sort(list));
		speedy.addAlgorithm(new ArrayBubbleSort());
		speedy.addAlgorithm(new ArraySelectionSort());
		speedy.addAlgorithm(new ArrayMergeSort());
	
		speedy.run();
		System.out.println("speed Tet finish...");
		
	}
}
