package at.jaykay.MagicSearch;
import java.util.ArrayList;

public interface SortAlgorithm {
	public ArrayList<Priority> sortPriority(ArrayList<Priority> list);
}
