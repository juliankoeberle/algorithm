package at.jaykay.MagicSearch;
import java.util.ArrayList;

public class MagicSortEngine {
	
	private SortAlgorithm algo;

	
	public MagicSortEngine(SortAlgorithm algo) {
		this.algo = algo;
	}
	
	public ArrayList<Priority> sortPriority(ArrayList<Priority> list){
		return algo.sortPriority(list);
	}
	

	public void setAlgo(SortAlgorithm algo) {
		this.algo = algo;
	}
	
	
	
	

}
