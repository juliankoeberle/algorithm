package at.jaykay.MagicSearch;
import java.util.ArrayList;

public class MagicSearch implements SearchAlgo{
	//selection sort
	@Override
	public ArrayList<String> searchList(ArrayList<String> list, String searchString) {
		String[] response = new String[list.size()];
		
		//analyse searchString
		//convert int to words
		char[] charArray = searchString.toCharArray();
		ArrayList<String> wordList = new ArrayList<String>();
		String word = "";
		for (int i = 0 ; i < charArray.length ; i++){
			if (charArray[i] != ' ' ){
				word = word + charArray[i];
			}
			else{
				wordList.add(word);
				word = "";
			}
		}
		wordList.add(word);
		
		//analyse String in list with words from wordlist
		//set priority
		int counter = 0;
		
		//save priority to sort 
		ArrayList<Priority> priorityList = new ArrayList<Priority>();
		for (String sentence : list){
			String text = sentence;
			//how many times a specific word is in a specific text
			//long word have more priority than short words
			
			int priority = -1;
			for (String wordString : wordList){
				//also count if the word exist more than one time
				if(text.contains(wordString)){
					priority = priority + wordString.length();
				}
			}
			System.out.println("[" + priority + "] " + sentence);
			//finally add a priority to priorityList
			priorityList.add(new Priority(counter,priority));
			counter++;
		}
		SortAlgorithm algo = new MagicSortAlgo();
		//sort by priority
		MagicSortEngine magicSortEngine = new MagicSortEngine(algo);
		//overwrite the old prioritylist
		priorityList = magicSortEngine.sortPriority(priorityList);
		
		//sort main list
		for (int i = 0 ; i < priorityList.size() ; i++){
			String str = list.get(priorityList.get(i).getIndex());
			int priority = priorityList.get(i).getPriority();
			response[i] = str;
		}
		
		System.out.println("-----------------------");
		System.out.println("input String:");
		System.out.println();
		for (String str : wordList){
			System.out.println(str);
		}
		System.out.println();
		System.out.println("-----------------------");
		list.clear();
		for (int i = 0 ; i < priorityList.size() ; i++){
			list.add(response[i]);
		}
		//analyse every String
		return list;
	}

}
