package at.jaykay.MagicSearch;
import java.util.ArrayList;

public interface SearchAlgo  {
	public ArrayList<String> searchList(ArrayList<String> list, String searchString);
}
