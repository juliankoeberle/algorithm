package at.jaykay.MagicSearch;
import java.util.ArrayList;

public class MagicSortAlgo implements SortAlgorithm {


	@Override
	public ArrayList<Priority> sortPriority(ArrayList<Priority> list) {
		
		ArrayList<Priority> newlist = new ArrayList<Priority>();
		//the size for the first loop should not be dynamic 
		//it should stay the same.
		int size = list.size();
		Priority prio = null;
		for (int i = 0; i < size; i++) {
			// search the smallest element in list 
			//get the first Priority of the list
			int biggestPriority = list.get(0).getPriority();
			int index = 0;
			
			for (int j = 0; j < list.size(); j++) {
				if (biggestPriority <= list.get(j).getPriority()) {
					prio = list.get(j);
					biggestPriority = prio.getPriority();
					index = prio.getIndex();
			
				}
				
			}
			// removes the the element from the old list
			list.remove(prio);
			// add to new list
			newlist.add(new Priority(index,biggestPriority));
		}
		return newlist;
	}

}
