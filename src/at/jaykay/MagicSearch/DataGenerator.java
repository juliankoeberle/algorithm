package at.jaykay.MagicSearch;
import java.util.ArrayList;
import java.util.Random;

public class DataGenerator {
	/*
	public static ArrayList<Integer> generateNumbers(int size, int minValue, int maxValue) {
		Random random = new Random();
		ArrayList<Integer> list = new ArrayList<Integer>();
		//only positve values could be generated
		if ((minValue >= 0) && (maxValue >= 0)) {
			for (int i = 0 ; i < size ; i++){
				//fills th list with the specific range.
				list.add(random.nextInt((maxValue - minValue) + 1) + minValue);
			}
		
		}
		return list;
	}
	*/
	public static ArrayList<String> generateStringList(){
		ArrayList<String> list = new ArrayList<String>();
		//random texts
		list.add("This is a text.");
		list.add("This is an other text");
		list.add("A substantial drum degenerates across the jargon.");
		list.add("The name defects?");
		list.add("The supernatural crawls beside a depressing heat.");
		list.add("The marginal incompetent fights the misprint.");
		list.add("A tenth tutorial praises the younger dictionary.");
		list.add("A blast bothers the fierce pin.");
		list.add("Every hope arches the quantum outside a joint professor.");
		list.add("The terminator jumps a drip.");
		list.add("How can every torture wave?");
		list.add("The taxi buffers any shout.");
		list.add("Will a league rattle?");
		list.add("An activating rose stirs beside the unseen.");
		list.add("An agreed parrot rolls around the arc.");
		list.add("A motorway digests a focus underneath the constitutional lifetime.");
		list.add("Why can't a clause compare a rational suffix?");
		list.add("A squared furniture updates a joy.");
		list.add("The downright anniversary gloves a bay.");
		list.add("The brass breaks the thankful pipeline.");
		
		return list;
	}
}
