package at.jaykay.MagicSearch;
import java.util.ArrayList;

public class BubbleMagicSearch implements SortAlgorithm{
	//Bubble Search
	@Override
	public ArrayList<Priority> sortPriority(ArrayList<Priority> list) {
		
		for (int i = 0 ; i<list.size(); i++){
			for (int j = 0 ; j<list.size(); j++){
				
				if (list.get(j).getPriority()<list.get(j+1).getPriority()){
					list.set(j, list.get(j));
				}
				else {
					list.set(j, list.get(j+1));
				}
			}
		}
		
		return null;
	}

}
