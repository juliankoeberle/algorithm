package at.jaykay.MagicSearch;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {
    /*
     * 
     * Algortihm by Julian Köberle 2016 all Copyrights reserved! 
     * this code is opensoruce;
     */
	public static void main(String[] args) {
		
	
		//At first make a new ArrayList with random Strings from "http://watchout4snakes.com/wo4snakes/Random/RandomSentence"
		ArrayList<String> searchList = DataGenerator.generateStringList();
		//uses the Selection search algortihm
		SearchAlgo magicSearch = new MagicSearch();
		String searchString = "This is a terminator rattle";
		printMagicSearch(magicSearch.searchList(searchList, searchString));

	}
 
	public static void printNumbers(ArrayList<Integer> list) {
		for (Integer number : list){
			System.out.println(number);
		}
		
	}
	//print the search list with the index of the String
	public static void printMagicSearch(ArrayList<String> list){
		//
		int i = 0;
		for (String str : list){
			System.out.println("["+i+"] "+str);
			i++;
		}
	}
}
