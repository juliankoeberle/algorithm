package at.jaykay.MagicSearch;

public class Priority {
	private int index;
	private int priority;
	
	
	
	public Priority(int index, int priority) {
		super();
		this.index = index;
		this.priority = priority;
	}
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
	
}
