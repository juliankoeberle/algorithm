package at.jaykay.Search;

import java.util.Arrays;

public class Main {
	
	//binary search
	public static void main(String[] args) {
		int[] array = {1,4,6,9,8,10,11,12};
		int searchNum = 9;
		//System.out.println(getIndex(searchNum, array));
		System.out.println(getIndexBySortedArray(searchNum,array));
	}
	//returns the index of the element in the array
	public static int getIndex(int number ,int[] array){
		int index = -1;
		for (int i = 0 ; i < array.length ; i++){
			if (number == array[i]){
				index = i;
				break;
			}
			
			
		}
		return index;
	}
	public static int getIndexBySortedArray(int number, int[] array){
		//sort array in short
		//java based code
		Arrays.sort(array);
		//go to the midle
		
		int i=2;
		int index = array.length/i;
		
		//num = 3
		//1,2/,3,4 /5,6,7,8,9
		while(true){
			//go left
			if (number < array[index]){
				i+=2;
				index = array.length/i; //2
			}
			else if (number == array[index]){
				break;
			}
			//go right
			else{
				index = index+ index/2;//3
			}
		}
		return index;
	}
	public static void  printArray(int[] array){
		for (int i = 0; i < array.length ;i++){
			System.out.print(array[i] + ",");
			System.out.println(" ");
		}
	}
}
