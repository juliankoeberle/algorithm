# README #

This is a simple search Algorithm.
By Julian Köberle
All Rights reserved.

### What is this repository for? ###

* everyone
* Version 0.1

### How do I get set up? ###

* open Eclipse
* new Project
* import from git


### How does the code work? ###

* At first it writes Random Text as a String in a list
* Then it's waiting for a input String
* This Inputstring will be converted in a several strings, separated with " " (Space).
* It scans every sentence if these words are contained and give this sentence a priority
* Long words have a higher priority than short ones
* Last but not leas it it sorts the list by the priority